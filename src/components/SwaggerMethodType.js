import React, {Component} from 'react';
import 'rc-collapse/assets/index.css';
import Collapse, {Panel} from 'rc-collapse';
import PropTypes from 'prop-types';
import '../Swagger.css';

class SwaggerMethodTypePrimitive extends Component {
    static PropTypes = {
        typeClass: PropTypes.string.isRequired,
        typeName: PropTypes.string.isRequired
    };

    render() {
        return <div className="primitiveType">
            <span className="elementType">{this.props.typeClass}</span>
        </div>
    }
}

class SwaggerMethodTypeArray extends Component {
    static PropTypes = {
        items: PropTypes.object.isRequired,
        typeName: PropTypes.string.isRequired,
        definitions: PropTypes.object.isRequired
    };

    render() {
        return <div className="arrayType">
            <span className="elementType">Array of <SwaggerMethodType type={this.props.items}
                                                                      definitions={this.props.definitions}/></span>
        </div>
    }
}

class SwaggerMethodTypeObject extends Component {
    static PropTypes = {
        typeName: PropTypes.string.isRequired,
        properties: PropTypes.object.isRequired,
        additionalProperties: PropTypes.object.isRequired,
        definitions: PropTypes.object.isRequired,
    };

    renderProperties() {
        const properties = {...this.props.properties, ...this.props.additionalProperties};
        return Object.keys(properties).map((propertyKey) => {
            const property = properties[propertyKey];
            return <tr>
                <td className="objectTypeName">{propertyKey}:</td>
                <td><SwaggerMethodType typeName={propertyKey} type={property} definitions={this.props.definitions}/>
                </td>
            </tr>
        });
    }

    curlyLeft = () => '{';
    curlyRight = () => '}';

    render() {
        return <div>
            <Collapse defaultActiveKey="1" className="foo">
                <Panel header={`Object (${this.props.typeName})`} key="1" className="objectType">
                    <table>
                        {this.renderProperties()}
                    </table>
                </Panel>
            </Collapse>
        </div>
    }
}


class SwaggerMethodType extends Component {
    static PropTypes = {
        typeName: PropTypes.string.isRequired,
        type: PropTypes.object.isRequired,
        definitions: PropTypes.object.isRequired
    };

    referencedDefinition() {
        const getRef = (refText) => {
            const definitionMatch = /^\#\/definitions\/(.*)$/.exec(refText);
            const definitionName = definitionMatch ? definitionMatch[1] : null;

            console.log("ref found: " + definitionName);
            return this.props.definitions[definitionName];
        };

        if (this.props.type.schema) {

            if (this.props.type.schema['$ref']) {
                return getRef(this.props.type.schema['$ref']);
            } else if (this.props.type.schema) {
                return this.props.type.schema;
            }
        } else if (this.props.type["$ref"]) {
            return getRef(this.props.type["$ref"]);
        }

        return null;
    }

    render() {
        const inVal = this.props.type.in;
        const type = this.referencedDefinition() || this.props.type;

        const typeRender = () => {
            switch (type.type) {
                case "integer":
                case "boolean":
                case "string":
                    return <SwaggerMethodTypePrimitive typeClass={type.type} typeName={this.props.typeName}/>;
                case "array":
                    console.log("array");
                    console.log(type);
                    return <SwaggerMethodTypeArray typeName={this.props.typeName} items={type.items}
                                                   definitions={this.props.definitions}/>;
                case "object":
                    return <SwaggerMethodTypeObject typeName={this.props.typeName} properties={type.properties}
                                                    additionalProperties={type.additionalProperties}
                                                    definitions={this.props.definitions}/>;
                default:
                    return <SwaggerMethodTypePrimitive typeClass={type.type} typeName={this.props.typeName}/>;
            }

            console.log("unknown type");
            console.log(type);

            return <div>no type: {type.type}</div>;
        };

        return (inVal === "body")
            ? typeRender()
            : typeRender()
    }
}


export
default
SwaggerMethodType
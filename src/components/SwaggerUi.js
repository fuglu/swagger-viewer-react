import React, {Component} from 'react';
import PropTypes from 'prop-types';
import '../Swagger.css';
import SwaggerMethodType from './SwaggerMethodType';


class RequestBodyContainer extends Component {
    render() {
        return <div>
            <h4 className="requestBodyContainer">REQUEST BODY</h4>
            {this.props.children}
        </div>
    }
}


class SwaggerPathMethodParam extends Component {
    static PropTypes = {
        parameter: PropTypes.object.isRequired,
        definitions: PropTypes.object.isRequired
    };

    renderParam() {
        return <tr>
            <td>
                <b>{this.props.parameter.name}:</b>
            </td>
            <td>
                <SwaggerMethodType typeName={this.props.parameter.name} type={this.props.parameter}
                                   definitions={this.props.definitions}/>
            </td>
        </tr>


    }

    render() {
        return this.props.parameter.in === "body"
            //? <RequestBodyContainer>{this.renderParam()}</RequestBodyContainer>
            ? this.renderParam()
            : this.renderParam()

    }


}

class SwaggerParamTable extends Component {
    render() {
        return <div>
            <table className="paramsTable">
                <tbody>{this.props.children}</tbody>
            </table>
        </div>
    }
}

class SwaggerPathMethodParams extends Component {
    static PropTypes = {
        parameters: PropTypes.arrayOf(PropTypes.object).isRequired,
        definitions: PropTypes.object.isRequired

    };

    renderParams() {
        return this.props.parameters
            ? this.props.parameters.map((param) => <SwaggerPathMethodParam parameter={param}
                                                                           definitions={this.props.definitions}/>)
            : null;
    }

    render() {
        return <div className="paramsTable"><h4>Parameters</h4>
            <SwaggerParamTable>{this.renderParams()}</SwaggerParamTable>
        </div>
    }
}

class SwaggerPathMethodResponse extends Component {
    static PropTypes = {
        code: PropTypes.string.isRequired,
        response: PropTypes.object.isRequired,
        definitions: PropTypes.object.isRequired

    };

    renderResponseSchema() {
        if (this.props.response.schema) {
            console.log(this.props.response.schema);
        }

        return this.props.response.schema
            ? <SwaggerMethodType typeName="" type={this.props.response.schema} definitions={this.props.definitions}/>
            : <div></div>;
    }

    render() {
        return <div className="responses">
            <div>{this.props.code}: {this.props.response.description}</div>
            {this.renderResponseSchema()}
        </div>
    }
}

class SwaggerPathMethodResponses extends Component {
    static PropTypes = {
        responses: PropTypes.object.isRequired,
        definitions: PropTypes.object.isRequired

    };

    renderResponses() {
        return this.props.responses
            ? Object.keys(this.props.responses).map((responseCode) => <SwaggerPathMethodResponse code={responseCode}
                                                                                                 response={this.props.responses[responseCode]}
                                                                                                 definitions={this.props.definitions}/>)
            : null;
    }

    render() {
        return <div className="paramsTable"><h4>Results</h4>
            <SwaggerParamTable>{this.renderResponses()}</SwaggerParamTable>
        </div>
    }
}

class SwaggerPathMethod extends Component {
    static PropTypes = {
        method: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        summary: PropTypes.string.isRequired,
        operationId: PropTypes.string.isRequired,
        tags: PropTypes.arrayOf(PropTypes.string).isRequired,
        parameters: PropTypes.arrayOf(PropTypes.object).isRequired,
        responses: PropTypes.object.isRequired,
        path: PropTypes.string.isRequired,
        definitions: PropTypes.object.isRequired

    };


    render() {

        console.log("responses");
        console.log(this.props.responses);

        return <div className="method">
            <div className="methodHeading">
                <span className="methodBox">{this.props.method}</span>
                <span className="methodPath">{this.props.path}</span>
                <span className="methodSummary">{this.props.summary || this.props.description}</span>
            </div>
            <div className="methodDescription">
                <span>{this.props.description || this.props.summary}</span>
            </div>
            <SwaggerPathMethodParams parameters={this.props.parameters} definitions={this.props.definitions}/>
            <SwaggerPathMethodResponses responses={this.props.responses} definitions={this.props.definitions}/>

        </div>;
    }

}

class SwaggerPath extends Component {
    static PropTypes = {
        methods: PropTypes.object.isRequired,
        name: PropTypes.string.isRequired,
        definitions: PropTypes.object.isRequired
    };

    /*
     <div class="opblock-tag-section is-open"><h4 class="opblock-tag"><span>pet</span><small>Everything about your Pets</small><button class="expand-operation" title="Expand operation"><svg class="arrow" width="20" height="20"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#large-arrow-down"></use></svg></button></h4><div style="height: auto; border: none; margin: 0px; padding: 0px;"><!-- react-text: 599 --> <!-- /react-text --><div class="opblock opblock-post" id="operations,post-/pet,pet"><div class="opblock-summary opblock-summary-post"><span class="opblock-summary-method">POST</span><span class="opblock-summary-path"><span>/pet</span><!-- react-empty: 605 --></span><div class="opblock-summary-description">Add a new pet to the store</div><button class="authorization__btn unlocked"><svg width="20" height="20"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#unlocked"></use></svg></button></div><!-- react-empty: 721 --></div><div class="opblock opblock-put" id="operations,put-/pet,pet"><div class="opblock-summary opblock-summary-put"><span class="opblock-summary-method">PUT</span><span class="opblock-summary-path"><span>/pet</span><!-- react-empty: 620 --></span><div class="opblock-summary-description">Update an existing pet</div><button class="authorization__btn unlocked"><svg width="20" height="20"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#unlocked"></use></svg></button></div><!-- react-empty: 722 --></div><div class="opblock opblock-get" id="operations,get-/pet/findByStatus,pet"><div class="opblock-summary opblock-summary-get"><span class="opblock-summary-method">GET</span><span class="opblock-summary-path"><span>/pet/findByStatus</span><!-- react-empty: 635 --></span><div class="opblock-summary-description">Finds Pets by status</div><button class="authorization__btn unlocked"><svg width="20" height="20"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#unlocked"></use></svg></button></div><!-- react-empty: 723 --></div><div class="opblock opblock-deprecated" id="operations,get-/pet/findByTags,pet"><div class="opblock-summary opblock-summary-get"><span class="opblock-summary-method">GET</span><span class="opblock-summary-path__deprecated"><span>/pet/findByTags</span><!-- react-empty: 650 --></span><div class="opblock-summary-description">Finds Pets by tags</div><button class="authorization__btn unlocked"><svg width="20" height="20"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#unlocked"></use></svg></button></div><!-- react-empty: 724 --></div><div class="opblock opblock-get" id="operations,get-/pet/{petId},pet"><div class="opblock-summary opblock-summary-get"><span class="opblock-summary-method">GET</span><span class="opblock-summary-path"><span>/pet/{petId}</span><!-- react-empty: 665 --></span><div class="opblock-summary-description">Find pet by ID</div><button class="authorization__btn unlocked"><svg width="20" height="20"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#unlocked"></use></svg></button></div><!-- react-empty: 725 --></div><div class="opblock opblock-post" id="operations,post-/pet/{petId},pet"><div class="opblock-summary opblock-summary-post"><span class="opblock-summary-method">POST</span><span class="opblock-summary-path"><span>/pet/{petId}</span><!-- react-empty: 680 --></span><div class="opblock-summary-description">Updates a pet in the store with form data</div><button class="authorization__btn unlocked"><svg width="20" height="20"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#unlocked"></use></svg></button></div><!-- react-empty: 726 --></div><div class="opblock opblock-delete" id="operations,delete-/pet/{petId},pet"><div class="opblock-summary opblock-summary-delete"><span class="opblock-summary-method">DELETE</span><span class="opblock-summary-path"><span>/pet/{petId}</span><!-- react-empty: 695 --></span><div class="opblock-summary-description">Deletes a pet</div><button class="authorization__btn unlocked"><svg width="20" height="20"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#unlocked"></use></svg></button></div><!-- react-empty: 727 --></div><div class="opblock opblock-post" id="operations,post-/pet/{petId}/uploadImage,pet"><div class="opblock-summary opblock-summary-post"><span class="opblock-summary-method">POST</span><span class="opblock-summary-path"><span>/pet/{petId}/uploadImage</span><!-- react-empty: 710 --></span><div class="opblock-summary-description">uploads an image</div><button class="authorization__btn unlocked"><svg width="20" height="20"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#unlocked"></use></svg></button></div><!-- react-empty: 728 --></div><!-- react-text: 720 --> <!-- /react-text --></div></div>
     */

    renderMethods() {
        const methods = this.props.methods;
        return Object.keys(this.props.methods).map((methodName) => <SwaggerPathMethod
            method={methodName} description={methods[methodName].description}
            operationId={methods[methodName].operationId}
            parameters={methods[methodName].parameters} responses={methods[methodName].responses}
            path={this.props.name}
            definitions={this.props.definitions}
            summary={methods[methodName].summary}
        />)
    }

    render() {
        console.log(this.props);
        return <div className="path">
            {this.renderMethods()}

        </div>;
    }
}

class SwaggerPaths extends Component {
    static PropTypes = {
        paths: PropTypes.arrayOf(PropTypes.object).isRequired,
        definitions: PropTypes.object.isRequired
    };

    renderPaths = (paths) => {
        console.log(paths);
        return Object.keys(paths).map((pathName) => {
            return <SwaggerPath name={pathName} methods={paths[pathName]} definitions={this.props.definitions}/>
        })
    };

    render() {
        return <div className="swaggerBase">{this.renderPaths(this.props.paths)}</div>;
    }
}

class SwaggerTag extends Component {
    static PropTypes = {
        name: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired
    };

    render() {
        return <div className="tag">
            <span className="tagHeading">{this.props.name}</span>
            <span className="tagDescription">{this.props.description}</span>
            <div>
                {this.props.children}
            </div>
        </div>
    }
}

export default class SwaggerUi extends Component {

    static PropTypes = {
        swaggerText: PropTypes.string.isRequired,
        tags: PropTypes.arrayOf(PropTypes.string).isRequired

    };

    tagFilter = (tagName) => (path) => {
        console.log(tagName);

        return Object.keys(path)
                .filter((method) => path[method].tags.indexOf(tagName) != -1).length > 0;
    };

    objectFilter = (filter) => (object) => {
        const filteredKeys = Object.keys(object)
            .filter((key) => filter(object[key]));

        const ret = {};

        console.log(Object.keys(object));
        console.log(filteredKeys);

        filteredKeys.forEach((key) => {
            ret[key] = object[key];
        });

        return ret;
    };

    renderTags(swaggerDoc) {

        const tagDescription = (name) => {
            const tags = swaggerDoc.tags.filter((tag) => tag.name === name)
            return tags.length > 0 ? tags[0].description : ""
        };

        const tagsToShow = (this.props.tags)
            ? this.props.tags.map((t) => ({name:t, description:tagDescription(t)}))
            : swaggerDoc.tags;

        return tagsToShow.map((tag) => (
            <SwaggerTag name={tag.name} description={tag.description}>
                <SwaggerPaths paths={this.objectFilter(this.tagFilter(tag.name))(swaggerDoc.paths)}
                              definitions={swaggerDoc.definitions}/>
            </SwaggerTag>
        ));
    }

    render() {
        console.log(this.swaggerDoc);


        const swaggerDoc = JSON.parse(this.props.swaggerText);

        return swaggerDoc.paths
            ? <div>
                {this.renderTags(swaggerDoc)}
            </div>
            : <div>empty doc</div>

    }


}
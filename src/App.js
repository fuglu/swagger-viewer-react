import React, {Component} from 'react';
import './App.css';
import SwaggerUi from "./components/SwaggerUi";

class App extends Component {

    state = { swaggerText: '{}' };

    swaggerText = `{}`;

    get = url =>
        fetch(`${url}`, {
            credentials: "include"
        });

//    parseJson = result => result.json();

    fetchJson = (filename) =>
        this.get(filename)


    setSwaggerText(json) {
        //console.log(json.text());
        console.log("setting");
        const self = this;
        json.text().then((text) => {
            this.setState({swaggerText: text});
        });

    }

    componentDidMount() {
        const self = this;
        this.fetchJson(this.props.swaggerUrl).then((json) => self.setSwaggerText(json));
    }

    render() {
        console.log(this.state.swaggerText);

        return <SwaggerUi swaggerText={this.state.swaggerText} tags={this.props.tags}/>;
    }
}

export default App;
